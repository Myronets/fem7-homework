// Реализовать класс Employee, в котором будут следующие свойства - name (имя), age (возраст), salary (зарплата). Сделайте так, чтобы эти свойства заполнялись при создании объекта.
// Сделайте геттеры и сеттеры для этих свойств.
// Сделайте класс Programmer, который будет наследоваться от класса Employee, у которого будет свойство lang (список языков)
// Для класса Programmer перезапишите геттер для свойства salary. Пусть он возвращает свойство salary умноженное на 3.
// Создайте несколько экземпляров обьекта Programmer, выведите их в консоль.

class Employee {
    constructor (name = "Vasia", age = 30, salery = 4200){
        this.name = name;
        this.age = age;
        this.salery  = salery;
    }
    get name() {
        return this._name
    }
    set name(valye) {
        return this._name = valye
    }
    get age() {
        return this._age
    }
    set age(valye) {
        return this._age = valye
    } 
    get salery() {
        return this._salery
    }
    set salery(valye) {
        return this._salery = valye
    }
}


class Programmer extends Employee {
    constructor (name, age, salery, lang = "eng"){
        super(name, age, salery);
        this.lang = lang;
    }
    get salery() {
        return this._salery*3
    }
    set salery(valye) {
        return this._salery = valye
    }
}

let petya = new Programmer("Petya");
let kolya = new Programmer("Kolya", 30, 5000, ["eng","china"]);
console.log(petya);
console.log(kolya)


