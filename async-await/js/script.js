const ipButton = document.querySelector('#ip_button');

ipButton.addEventListener('click', ev =>{
    getAdress().then(result => addContent(result));
});

async function getAdress(
    userIpUrl = 'https://api.ipify.org/?format=json',
    userAdressUrl = 'http://ip-api.com/json/',
    fields = '?fields=status,message,continent,country,regionName,city,district,query'
    ) {
    const responseIP = await fetch(userIpUrl);
    const userIp = await responseIP.json();

    const responseAdress = await fetch(userAdressUrl + userIp.ip + fields);
    const userAdress = await responseAdress.json();

    return userAdress;
}

function addContent (obj){
    let content = document.createElement('div');
    content.innerHTML = `<p>континент: ${obj.continent}</p>
                        <p>страна: ${obj.country}</p>
                        <p>регион: ${obj.regionName}</p>
                        <p>город: ${obj.city}</p>
                        <p>район города: ${obj.district}</p>`;
    document.body.appendChild(content);
}

