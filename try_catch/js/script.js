// Дан массив books.
const books = [
    {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70
    },
    {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
    },
    {
    name: "Тысячекратная мысль",
    price: 70
    },
    {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70
    },
    {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40
    },
    {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
    }
    ];
    
    // Выведите этот массив в виде списка (тег ul).
    // На странице должен находится div с id="root", куда и нужно будет положить этот список (похожая задача была дана в модуле basic).
    // Перед выводом обьекта на странице, нужно проверить его на корректность (в обьекте должны содержаться все три свойства - author, name, price). Если какого-то из этих свойств нету, в консоли должна высветится ошибка с указанием - какого свойства нету в обьекте.
    // Тот обьект, что некорректен по условиям предыдущего пункта - не должен появиться на странице.
    
      
    function createList(array) {
        const newArray = [];
        for (let i=0; i<array.length; i++){
            try {
                if (!array[i].name){throw new Error(`Element ${i} has not property "name"`)}
                if (!array[i].author){throw new Error(`Element ${i} has not property "autor"`)}
                if (!array[i].price){throw new Error(`Element ${i} has not property "price"`)}
    
                newArray.push(`<li><p>${array[i].author}</p><p>${array[i].name}</p><p>${array[i].price}</p></li>`)
            }
            catch(e){
                console.log(e)
            }
            
        }

        let root = document.getElementById("root");
        ul = document.createElement('ul');
          
        root.appendChild(ul);
        ul.innerHTML = newArray.join('');
    
    }
    
    createList(books);