const ipButton = document.querySelector('#ip_button');

ipButton.addEventListener('click', ev =>{
    fetch('https://api.ipify.org/?format=json')
    .then((responce) =>{
        if (!responce.ok){throw new Error(response.statusText)}
        else return responce.json();
    })
    .then((result) => {
        fetch(`http://ip-api.com/json/${result.ip}?fields=status,message,continent,country,regionName,city,district,query`)
        .then(response => {
        if (!response) throw new Error(response.statusText);
        else return response.json();
        }).then((result)=>{
            let adress = document.createElement('div');
            adress.innerHTML = `<p>континент: ${result.continent}</p>
                                <p>страна: ${result.country}</p>
                                <p>регион: ${result.regionName}</p>
                                <p>город: ${result.city}</p>
                                <p>район города: ${result.district}</p>`;
            document.body.appendChild(adress);
        })
    })
    .catch((error) => {
        console.log(error);
    });
});